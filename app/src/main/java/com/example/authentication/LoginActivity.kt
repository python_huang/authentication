package com.example.authentication

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth? = null
    private var accoutLayout: TextInputLayout? = null
    private var passwordLayout: TextInputLayout? = null
    private var accountEdit: EditText? = null
    private var passwordEdit: EditText? = null
    private var loginBtn: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }

    private fun initView() {
        mAuth = FirebaseAuth.getInstance()
        accountEdit = findViewById<View>(R.id.account_edit) as EditText
        passwordEdit = findViewById<View>(R.id.password_edit) as EditText
        accoutLayout = findViewById<View>(R.id.account_layout) as TextInputLayout
        passwordLayout = findViewById<View>(R.id.password_layout) as TextInputLayout
        passwordLayout!!.isErrorEnabled = true
        accoutLayout!!.isErrorEnabled = true
        loginBtn = findViewById<View>(R.id.login_button) as Button
        loginBtn!!.setOnClickListener(View.OnClickListener {
            val account = accountEdit!!.text.toString()
            val password = passwordEdit!!.text.toString()
            if (TextUtils.isEmpty(account)) {
                accoutLayout!!.error = getString(R.string.plz_input_accout)
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password)) {
                passwordLayout!!.error = getString(R.string.plz_input_pw)
                return@OnClickListener
            }
            accoutLayout!!.error = ""
            passwordLayout!!.error = ""
            mAuth!!.signInWithEmailAndPassword(account, password)
                .addOnCompleteListener(this@LoginActivity) { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this@LoginActivity, R.string.login_success, Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent()
                        intent.setClass(this@LoginActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@LoginActivity, task.exception!!.message, Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        })
    }
}