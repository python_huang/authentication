package com.example.authentication

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class SignUpActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth? = null
    private var accoutLayout: TextInputLayout? = null
    private var passwordLayout: TextInputLayout? = null
    private var accountEdit: EditText? = null
    private var passwordEdit: EditText? = null
    private var signUpBtn: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        initView()
    }

    private fun initView() {
        mAuth = FirebaseAuth.getInstance()
        accountEdit = findViewById<View>(R.id.account_edit) as EditText
        passwordEdit = findViewById<View>(R.id.password_edit) as EditText
        accoutLayout = findViewById<View>(R.id.account_layout) as TextInputLayout
        passwordLayout = findViewById<View>(R.id.password_layout) as TextInputLayout
        passwordLayout!!.isErrorEnabled = true
        accoutLayout!!.isErrorEnabled = true
        signUpBtn = findViewById<View>(R.id.signup_button) as Button
        signUpBtn!!.setOnClickListener(View.OnClickListener {
            val account = accountEdit!!.text.toString()
            val password = passwordEdit!!.text.toString()
            if (TextUtils.isEmpty(account)) {
                accoutLayout!!.error = getString(R.string.plz_input_accout)
                passwordLayout!!.error = ""
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password)) {
                accoutLayout!!.error = ""
                passwordLayout!!.error = getString(R.string.plz_input_pw)
                return@OnClickListener
            }
            accoutLayout!!.error = ""
            passwordLayout!!.error = ""
            mAuth!!.createUserWithEmailAndPassword(account, password)
                .addOnCompleteListener(this@SignUpActivity) { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this@SignUpActivity, R.string.register_success, Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent()
                        intent.setClass(this@SignUpActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@SignUpActivity, task.exception!!.message, Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        })
    }
}