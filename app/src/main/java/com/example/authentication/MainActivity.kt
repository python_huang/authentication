package com.example.authentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity

import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private var logout: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        logout = findViewById<View>(R.id.logout) as Button
        logout!!.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent()
            intent.setClass(this@MainActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}