package com.example.authentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity

import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {
    private var login: Button? = null
    private var signUp: Button? = null
    private var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAuth = FirebaseAuth.getInstance()
        val user = mAuth!!.currentUser
        if (user == null) {
            setContentView(R.layout.activity_home)
            login = findViewById<View>(R.id.login) as Button
            signUp = findViewById<View>(R.id.sign_up) as Button

            login!!.setOnClickListener {
                val intent = Intent()
                intent.setClass(this@HomeActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
            signUp!!.setOnClickListener {
                val intent = Intent()
                intent.setClass(this@HomeActivity, SignUpActivity::class.java)
                startActivity(intent)
                finish()
            }
        } else {
            val intent = Intent()
            intent.setClass(this@HomeActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}